package app;

import app.model.Car;
import app.model.Crossrode;
import app.model.LightBulbs;
import app.model.Orientation;
import app.model.Road;
import app.model.RoadAdditionWrapper;
import app.model.RoadService;
import app.model.TrafficLights;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;

import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;

public class Controller implements Initializable {

    @FXML
    GridPane grid;

    @FXML
    Label timeLabel;

    @FXML
    Label carsLabel;

    @FXML
    Circle light00;
    @FXML
    Circle light01;
    @FXML
    Circle light02;
    @FXML
    Circle light03;

    @FXML
    Circle light10;
    @FXML
    Circle light11;
    @FXML
    Circle light12;
    @FXML
    Circle light13;

    @FXML
    Circle light20;
    @FXML
    Circle light21;
    @FXML
    Circle light22;
    @FXML
    Circle light23;

    Set<Car> carsSet = new HashSet<>();
    List<TrafficLights> trafficLights = new ArrayList<>();
    List<Point> spotPoints = new ArrayList<>();
    Set<Crossrode> crossrodes = new HashSet<Crossrode>();
    RoadService roadService;
    Set<Point> occupiedPosition = new HashSet<>();

    private static final int AFTER_HOW_MUCH_CAR = 8;
    private static final float TIME_FACTOR = 8f;

    Task task = new Task<Void>() {
        @Override
        public Void call() {
            boolean stop = false;

            while (!stop) {
                if (getTime() % AFTER_HOW_MUCH_CAR == 0) {
                    addNewCar();
                    Platform.runLater(() -> resolveCarSet());
                }
                if (getTime() % 2 == 0) {
                    trafficLights.forEach(TrafficLights::yellowFlash);
                }
                try {
                    Thread.sleep((long) (1000 / TIME_FACTOR));
                    Platform.runLater(() -> incrementTime());
                    moveCars(grid);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
            return null;
        }
    };

    public void initialize(URL location, ResourceBundle resources) {

        grid.setAlignment(Pos.CENTER);
//        light00.setFill(LightBulbs.RED.getPaint());
//        light01.setFill(LightBulbs.GREEN.getPaint());
//        light02.setFill(LightBulbs.GREEN.getPaint());
//        light03.setFill(LightBulbs.RED.getPaint());
        light00.setFill(LightBulbs.ORANGE.getPaint());
        light01.setFill(LightBulbs.ORANGE.getPaint());
        light02.setFill(LightBulbs.ORANGE.getPaint());
        light03.setFill(LightBulbs.ORANGE.getPaint());

        TrafficLights trafficLights0 = new TrafficLights(light00, light01, light02, light03, 20, 7);
        trafficLights.add(trafficLights0);

//        light10.setFill(LightBulbs.RED.getPaint());
//        light11.setFill(LightBulbs.GREEN.getPaint());
//        light12.setFill(LightBulbs.GREEN.getPaint());
//        light13.setFill(LightBulbs.RED.getPaint());
        light10.setFill(LightBulbs.ORANGE.getPaint());
        light11.setFill(LightBulbs.ORANGE.getPaint());
        light12.setFill(LightBulbs.ORANGE.getPaint());
        light13.setFill(LightBulbs.ORANGE.getPaint());

        TrafficLights trafficLights1 = new TrafficLights(light10, light11, light12, light13, 20, 15);

        trafficLights.add(trafficLights1);
//
//        light20.setFill(LightBulbs.RED.getPaint());
//        light21.setFill(LightBulbs.GREEN.getPaint());
//        light22.setFill(LightBulbs.GREEN.getPaint());
//        light23.setFill(LightBulbs.RED.getPaint());

        light20.setFill(LightBulbs.ORANGE.getPaint());
        light21.setFill(LightBulbs.ORANGE.getPaint());
        light22.setFill(LightBulbs.ORANGE.getPaint());
        light23.setFill(LightBulbs.ORANGE.getPaint());

        TrafficLights trafficLights2 = new TrafficLights(light20, light21, light22, light23, 20, 24);
        trafficLights.add(trafficLights2);

        spotPoints.add(new Point(0, 8));
        spotPoints.add(new Point(0, 16));
        spotPoints.add(new Point(0, 25));

        spotPoints.add(new Point(21, 31));
        spotPoints.add(new Point(20, 0));

        spotPoints.add(new Point(31, 7));
        spotPoints.add(new Point(31, 15));
        spotPoints.add(new Point(31, 24));

        Road road0 = new Road(trafficLights0, new int[]{7, 8}, Orientation.HORIZONTAL, occupiedPosition);
        Road road1 = new Road(trafficLights1, new int[]{15, 16}, Orientation.HORIZONTAL, occupiedPosition);
        Road road2 = new Road(trafficLights2, new int[]{24, 25}, Orientation.HORIZONTAL, occupiedPosition);
        Road road3 = new Road(trafficLights2, new int[]{20, 21}, Orientation.VERTICAL, occupiedPosition);
        Set<Road> roads = new HashSet<>();
        roads.add(road0);
        roads.add(road1);
        roads.add(road2);
        roads.add(road3);

        Crossrode c1 = new Crossrode(road0, road3, 20, 7);
        Crossrode c2 = new Crossrode(road1, road3, 20, 15);
        Crossrode c3 = new Crossrode(road2, road3, 20, 24);
        crossrodes.add(c1);
        crossrodes.add(c2);
        crossrodes.add(c3);

        roadService = new RoadService(roads, crossrodes);
        new Thread(task).start();
    }

    private void incrementTime() {
        int time = getTime();
        time++;
        timeLabel.setText(String.valueOf(time));
    }

    private void resolveCarSet() {
        carsLabel.setText(String.valueOf(carsSet.size()));

    }

    private int getTime() {
        return Integer.parseInt(timeLabel.getText());
    }


    private void addNewCar() {
        Point p = getNewSpotPoint();
        Car car = new Car(p.x, p.y, roadService, occupiedPosition);
        carsSet.add(car);
        RoadAdditionWrapper wrapper = roadService.findByPosition(p.x, p.y);
        wrapper.getRoad().addCar(car, wrapper.isRight());
    }

    private Point getNewSpotPoint() {
        Random random = new Random();
        int i = random.nextInt(spotPoints.size());
        return spotPoints.get(i);
    }


    private void moveCars(GridPane grid) {
        roadService.move();
        removeCars();
        List<Node> toDelete = grid.getChildren().filtered(x -> x.getTypeSelector().equals("Rectangle"));

        Platform.runLater(() -> {
            grid.getChildren().removeAll(toDelete);
            carsSet.forEach(car -> {
                grid.add(car.getRectangle(), car.getX(), car.getY());
            });
        });
    }

    private void removeCars() {
        carsSet.removeIf(car -> car.isToRemove());
        Platform.runLater(() -> resolveCarSet());
    }
}
