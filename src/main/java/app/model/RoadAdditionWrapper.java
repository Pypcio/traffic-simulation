package app.model;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class RoadAdditionWrapper {

    private Road road ;
    boolean right;

    public RoadAdditionWrapper(Road road, boolean right) {
        this.road = road;
        this.right = right;
    }

    public Road getRoad() {
        return road;
    }

    public boolean isRight() {
        return right;
    }
}
