package app.model;

import javafx.scene.paint.Paint;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public enum LightBulbs {
    RED("red"), GREEN("GREEN"), ORANGE("ORANGE"), GRAY("GRAY");

    String value;
    Paint paint;

    LightBulbs(String value) {
        this.value = value;
        this.paint = Paint.valueOf(value);
    }

    public String getValue() {
        return value;
    }

    public Paint getPaint() {
        return paint;
    }
}
