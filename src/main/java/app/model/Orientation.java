package app.model;

import javafx.scene.paint.Paint;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public enum Orientation {
    HORIZONTAL("horizontal"), VERTICAL("vertical");

    String value;

    Orientation(String value) {
        this.value = value;
    }
}
