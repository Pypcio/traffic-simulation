package app.model;

import javafx.scene.shape.Circle;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class TrafficLights {

    Circle leftUp;
    Circle rightUp;
    Circle leftDown;
    Circle rightDown;
    int[] position = new int[2];
    Set<Circle> circleSet = new HashSet<>();

    public TrafficLights(Circle leftUp, Circle rightUp, Circle leftDown, Circle rightDown, int x, int y) {
        this.leftUp = leftUp;
        this.rightUp = rightUp;
        this.leftDown = leftDown;
        this.rightDown = rightDown;

        circleSet.add(leftUp);
        circleSet.add(rightUp);
        circleSet.add(leftDown);
        circleSet.add(rightDown);
        position[0] = x;
        position[1] = y;

    }

    public void changeState(){
        circleSet.forEach(x -> {
                if(x.getFill().equals(LightBulbs.GREEN.getPaint())){
                    x.setFill(LightBulbs.RED.getPaint());
                } else {
                    x.setFill(LightBulbs.GREEN.getPaint());
                }
        });
    }

    public void yellowFlash(){
        circleSet.forEach(x -> {
                if(x.getFill().equals(LightBulbs.ORANGE.getPaint())){
                    x.setFill(LightBulbs.GRAY.getPaint());
                } else {
                    x.setFill(LightBulbs.ORANGE.getPaint());
                }
        });
    }

    public boolean allowRide(Orientation orientation){
        if (orientation.equals(Orientation.HORIZONTAL)){
            return LightBulbs.GREEN.getPaint().equals(leftDown.getFill());
        } else {
            return LightBulbs.GREEN.equals(leftUp.getFill());
        }
    }
}
