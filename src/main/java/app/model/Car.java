package app.model;


import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.awt.*;
import java.util.Random;
import java.util.Set;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class Car {

    Move nextMove;
    Rectangle rectangle;
    Road road;
    int x;
    int y;
    boolean toRemove = false;
    boolean isWaintingForGreen = false;
    int turn = 0;
    boolean isOnCrossroad = false;

    public Car(int x, int y, RoadService service, Set<Point> occupedPosition) {
        this.nextMove = getMove();
        this.rectangle = new Rectangle(x, y, 14, 14);
        this.x = x;
        this.y = y;
        rectangle.setFill(getRandomPaint());
        road = service.findByPosition(x, y).getRoad();
        occupedPosition.add(new Point(x, y));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private Paint getRandomPaint() {
        Random random = new Random();
        int gen = random.nextInt(8);

        switch (gen) {
            case 0:
                return Paint.valueOf("BLACK");
            case 1:
                return Paint.valueOf("CYAN");
            case 2:
                return Paint.valueOf("YELLOW");
            case 3:
                return Paint.valueOf("RED");
            case 4:
                return Paint.valueOf("GREEN");
            case 5:
                return Paint.valueOf("ORANGE");
            case 6:
                return Paint.valueOf("PINK");
            case 7:
                return Paint.valueOf("GRAY");
            default:
                return Paint.valueOf("WHITE");
        }
    }

    public Move getNextMove() {
        return nextMove;
    }

    public void setNextMove(Move nextMove) {
        this.nextMove = nextMove;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setOnCrossroad(boolean onCrossroad) {
        isOnCrossroad = onCrossroad;
    }

    public void move(Orientation orientation, boolean rightLine, Set<Point> occupedPosition) {
        if (turn == 1) {
            moveCross(orientation, rightLine, occupedPosition);
        } else {
            moveForward(orientation, rightLine, occupedPosition);
        }
        if (turn == 1) {
            turn = 0;
        }
    }


    public void moveForward(Orientation orientation, boolean rightLine, Set<Point> occupedPosition) {
        if (rightLine) {
            if (Orientation.HORIZONTAL.equals(orientation)) {
                if (!occupedPosition.contains(new Point(x + 1, y))) {
                    occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                    x++;
                    occupedPosition.add(new Point(x, y));
                }
            } else {
                if (!occupedPosition.contains(new Point(x, y - 1))) {
                    occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                    y--;
                    occupedPosition.add(new Point(x, y));
                }
            }
        } else {
            if (Orientation.HORIZONTAL.equals(orientation)) {
                if (!occupedPosition.contains(new Point(x - 1, y))) {
                    occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                    x--;
                    occupedPosition.add(new Point(x, y));
                }
            } else {
                if (!occupedPosition.contains(new Point(x, y + 1))) {
                    occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                    y++;
                    occupedPosition.add(new Point(x, y));
                }
            }
        }
    }

    public void moveCross(Orientation orientation, boolean rightLine, Set<Point> occupedPosition) {
        if (rightLine) {
            if (Orientation.HORIZONTAL.equals(orientation)) {
                switch (nextMove) {
                    case HEAD:
                        if (!occupedPosition.contains(new Point(x + 1, y))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case LEFT:
                        if (!occupedPosition.contains(new Point(x + 1, y - 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x++;
                            y--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case RIGHT:
                        if (!occupedPosition.contains(new Point(x, y + 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            y++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                }
            } else {
                switch (nextMove) {
                    case HEAD:
                        if (!occupedPosition.contains(new Point(x, y - 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            y--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case LEFT:
                        if (!occupedPosition.contains(new Point(x - 1, y - 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x--;
                            y--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case RIGHT:
                        if (!occupedPosition.contains(new Point(x + 1, y))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                }
            }
        } else {
            if (Orientation.HORIZONTAL.equals(orientation)) {
                switch (nextMove) {
                    case HEAD:
                        if (!occupedPosition.contains(new Point(x - 1, y))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case LEFT:
                        if (!occupedPosition.contains(new Point(x - 1, y + 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x--;
                            y++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case RIGHT:
                        if (!occupedPosition.contains(new Point(x, y - 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            y--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                }
            } else {
                switch (nextMove) {
                    case HEAD:
                        if (!occupedPosition.contains(new Point(x, y + 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            y++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case LEFT:
                        if (!occupedPosition.contains(new Point(x + 1, y + 1))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x++;
                            y++;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                    case RIGHT:
                        if (!occupedPosition.contains(new Point(x - 1, y))) {
                            occupedPosition.remove(occupedPosition.stream().filter(op -> op.getX() == x && op.getY() == y).findFirst().get());
                            x--;
                            occupedPosition.add(new Point(x, y));
                        }
                        break;
                }
            }
        }
        nextMove = getMove();
        turn = 1;
    }


    public boolean isToRemove() {
        return toRemove;
    }

    public void setToRemove(boolean toRemove) {
        this.toRemove = toRemove;
    }

    private Move getMove() {
        Random random = new Random();
        int i = random.nextInt(3);
        switch (i) {
            case 1:
                return Move.RIGHT;
            case 2:
                return Move.LEFT;
            default:
                return Move.HEAD;
        }
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public Road getRoad() {
        return road;
    }

    public void setRoad(Road road) {
        this.road = road;
    }
}

