package app.model;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class Crossrode {

    Road horizontal;
    Road vertical;
    int x;
    int y;

    public Crossrode(Road horizontal, Road vertical, int x, int y) {
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.x = x;
        this.y = y;
    }

    public Road getHorizontal() {
        return horizontal;
    }

    public Road getVertical() {
        return vertical;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
