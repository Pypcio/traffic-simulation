package app.model;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public enum Move {
    HEAD("head"), LEFT("left"), RIGHT("rigth");


    String value;

    Move(String s) {
        this.value = s;
    }
}
