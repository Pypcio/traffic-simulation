package app.model;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class SpotPoint {
    int x;
    int y;

    public SpotPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
