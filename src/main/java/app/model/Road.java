package app.model;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class Road {

    TrafficLights lights;
    List<Car> carsRight;
    List<Car> carsLeft;
    int[] position = new int[2];
    private Orientation orientation;
    Set<Point> occupedPoins;

    public Road(TrafficLights lights, int[] position, Orientation orientation, Set<Point> occupedPoins) {
        this.lights = lights;
        this.carsLeft = new LinkedList<>();
        this.carsRight = new LinkedList<>();
        this.position = position;
        this.orientation = orientation;
        this.occupedPoins = occupedPoins;
    }

    public void addCar(Car car, boolean right) {
        if (right) {
            carsRight.add(car);
        } else {
            carsLeft.add(car);
        }
    }

    public void move(Set<Crossrode> crossrode) {

        carsLeft.stream().filter(car -> car.isOnCrossroad && car.getTurn() == 0 && !car.nextMove.equals(Move.HEAD)).forEach(car -> {
            Crossrode cr = crossrode.stream().filter(crossrode1 -> crossrode1.getX() == lights.position[0] && crossrode1.getY() == lights.position[1]).findFirst().get();
            if (Orientation.HORIZONTAL.equals(orientation)) {
                if (car.getNextMove().equals(Move.LEFT)) {
                    cr.getVertical().addCar(car, false);
                } else {
                    cr.getVertical().addCar(car, true);
                }
                car.setRoad(cr.getVertical());
                car.setTurn(1);
                car.setOnCrossroad(false);

            } else {
                if (car.getNextMove().equals(Move.LEFT)) {
                    cr.getHorizontal().addCar(car, true);
                } else {
                    cr.getHorizontal().addCar(car, false);
                }
                car.setRoad(cr.getHorizontal());
                car.setTurn(1);
                car.setOnCrossroad(false);

            }
        });

        carsRight.stream().filter(car -> car.isOnCrossroad && car.getTurn() == 0 && !car.nextMove.equals(Move.HEAD)).forEach(car -> {
            Crossrode cr = crossrode.stream().filter(crossrode1 -> crossrode1.getX() == lights.position[0] && crossrode1.getY() == lights.position[1]).findFirst().get();
            if (Orientation.HORIZONTAL.equals(orientation)) {
                if (car.getNextMove().equals(Move.LEFT)) {
                    cr.getVertical().addCar(car, true);
                } else if (car.getNextMove().equals(Move.RIGHT)) {
                    cr.getVertical().addCar(car, false);
                }
                car.setTurn(1);
                car.setRoad(cr.getVertical());
                car.setOnCrossroad(false);
            } else {
                if (car.getNextMove().equals(Move.LEFT)) {
                    cr.getHorizontal().addCar(car, false);
                } else if (car.getNextMove().equals(Move.RIGHT)) {
                    cr.getHorizontal().addCar(car, true);
                }
                car.setRoad(cr.getHorizontal());
                car.setTurn(1);
                car.setOnCrossroad(false);
            }
        });

        if (orientation.equals(Orientation.HORIZONTAL)) {
            carsLeft.forEach(car -> car.move(Orientation.HORIZONTAL, false, occupedPoins));
            carsRight.forEach(car -> car.move(Orientation.HORIZONTAL, true, occupedPoins));
        } else if (orientation.equals(Orientation.VERTICAL)) {
            carsLeft.forEach(car -> car.move(Orientation.VERTICAL, false, occupedPoins));
            carsRight.forEach(car -> car.move(Orientation.VERTICAL, true, occupedPoins));
        }

        carsRight.removeIf(car -> !car.getRoad().equals(this));
        carsLeft.removeIf(car -> !car.getRoad().equals(this));

        carsLeft.stream().filter(car -> car.getX() == lights.position[0]  && car.getY() == lights.position[1] && car.getTurn() == 0).forEach(car ->
                car.setOnCrossroad(true)
        );

        carsRight.stream().filter(car -> car.getX() == lights.position[0] + 1 && car.getY() == lights.position[1] + 1 && car.getTurn() == 0).forEach(car ->
                car.setOnCrossroad(true)
        );

        carsLeft.stream().filter(car -> car.getX() == lights.position[0] + 1 && car.getY() == lights.position[1] && car.getTurn() == 0).forEach(car ->
                car.setOnCrossroad(true)
        );

        carsRight.stream().filter(car -> car.getX() == lights.position[0] && car.getY() == lights.position[1] + 1 && car.getTurn() == 0).forEach(car ->
                car.setOnCrossroad(true)
        );

        carsLeft.stream().filter(car -> car.getX() < 0 || car.getY() < 0 || car.getX() > 31 || car.getY() > 31).forEach(car -> car.setToRemove(true));
        carsRight.stream().filter(car -> car.getX() < 0 || car.getY() < 0 || car.getX() > 31 || car.getY() > 31).forEach(car -> car.setToRemove(true));

        carsLeft.removeIf(car -> car.getX() < 0 || car.getY() < 0 || car.getX() > 31 || car.getY() > 31);
        carsRight.removeIf(car -> car.getX() < 0 || car.getY() < 0 || car.getX() > 31 || car.getY() > 31);

        occupedPoins.removeIf(p -> p.getY()>31 || p.getY() < 0 || p.getX() > 31 || p.getX() <0);
    }

}
