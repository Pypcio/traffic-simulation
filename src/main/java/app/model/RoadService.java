package app.model;

import java.util.Set;

/**
 * Created by Szymon Pypłacz on 2018-06-09.
 */
public class RoadService {

    Set<Road> roads;
    Set<Crossrode> crossrodes;

    public RoadService(Set<Road> roads, Set<Crossrode> crossrodes) {
        this.roads = roads;
        this.crossrodes = crossrodes;
    }


    public void setRoads(Set<Road> roads) {
        this.roads = roads;
    }

    public RoadAdditionWrapper findByPosition(int x, int y){
        Road r;
        boolean isOnRigthLine;
        if (x == 0 || x == 31){
            r = roads.stream().filter(l -> l.position[0] == y || l.position[1] == y).findFirst().get();
            isOnRigthLine = r.position[1] == y;
        } else {
            r = roads.stream().filter(l -> l.position[0] == x || l.position[1] == x).findFirst().get();
            isOnRigthLine = r.position[1] == x;
        }

        RoadAdditionWrapper wrapper = new RoadAdditionWrapper(r, isOnRigthLine);
        return wrapper;
    }

    public void move(){
        roads.forEach(r -> r.move(crossrodes));
    }
}
